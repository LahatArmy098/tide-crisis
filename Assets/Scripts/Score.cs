﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    public static int scoreAmount;
    Text scoreText;

    // Use this for initialization
    void Start()
    {
        scoreText = GetComponent<Text>();
        scoreAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = scoreAmount + "/1000 ";
        if (scoreAmount < 1000)
        {
            //Time.timeScale = 1;
        }
        else
        {
            SceneManager.LoadScene("Win");
        }
    }
}
